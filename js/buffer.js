/*

Start by creating a buffer.

On load add content from buffer to editors.
- load content from specific file; store in memory.
- display stored content in editors
- upon changes in editors update stored memory
Problem: Memory duplication (buffer is the same as editor tab, trivial in this instance. May not ultimately be a good design pattern, thouhg.) Allocated space must be available for preserving file changes while changing tabs.

On close editor tab (switch tab)
- Preserve data in buffer, replace with other buffer.

On save:
– Create file object from buffer. (Triple memory usage now... this is started to seem like a very bad design pattern.) Save file to disk. (Temporary location, so not uploaded to site master; Create secondary section on sidebar for files which are not to be uploaded yet.)

On close file:
- Wipe specific buffer. Replace contents with 'null,';


*/


/* const fs = require('fs'); */

let data = {
    metadata: {
        layout: '',
        title: '',
        description: '',
        date: '',
        categories: '',
        tags: '',
        slug: ''
    },
    editorContent: ''
};

function create() {

}

function load(file) {
    let dataString = loadFile(file);
    populateData(dataString);
}

function save() {
    let file = createFile();
    writeFile(file);
}

function clear() {
    data = null, 0;
}

function update(data) {
    buffer.data = data;
}

function populateData(dataString) {
    data.metadata.layout = dataString.split('---')[1].split('\n')[1].split(': ')[1] || '';
    data.metadata.title = dataString.split('---')[1].split('\n')[2].split(': ')[1] || '';
    data.metadata.description = dataString.split('---')[1].split('\n')[3].split(': ')[1] || '';
    data.metadata.date = dataString.split('---')[1].split('\n')[4].split(': ')[1] || '';
    data.metadata.categories = dataString.split('---')[1].split('\n')[5].split(': ')[1] || '';
    data.metadata.tags = dataString.split('---')[1].split('\n')[6].split(': ')[1] || '';
    data.metadata.slug = dataString.split('---')[1].split('\n')[7].split(': ')[1] || '';

    data.editorContent = dataString.split('---')[2] || '';
    return data;
}

function createFile() {
    let dataString = '---\n' + 
    'layout: ' + data.metadata.layout + '\n' + 
    'title: ' + data.metadata.title + '\n' + 
    'description: ' + data.metadata.description + '\n' + 
    'date: ' + data.metadata.date + '\n' + 
    'categories: ' + data.metadata.categories + '\n' + 
    'tags: ' + data.metadata.tags + '\n' +
    'slug: ' + data.metadata.slug + '\n' +
    '---\n' +
    data.editorContent;

    return dataString;
}

function loadFile(file) {
    /* fs.readFile(file, (err, data) => {
    	if (err) throw err;
		return data;
    }); */
}

function writeFile(file) {
    let filename = data.metadata.slug + '.html';
    /* fs.writeFile(filename, file, (err) => {
    	if (err) throw err;
    	console.log('The file has been saved!');
    }); */
}