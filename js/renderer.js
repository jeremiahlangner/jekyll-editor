/* const fileBuffer = require('./buffer'); */

function value(el) {
    return document.querySelector(el).value || '';
}

function is_all_ws(nod)
{
    return !(/[^\t\n\r ]/.test(nod.textContent));
}

function is_ignorable(nod) {
    return (nod.nodeType == 8) || 
    ((nod.nodeType == 3) && is_all_ws(nod)); 
}

function previousElementSibling(el) {
    while ((el = el.previousSibling)) {
        if (!is_ignorable(el)) return el;
    }
    return null;
}

function skipSib(el, i) {
    var targetEl = el.nextElementSibling;
    for(var j=1; j <= i; j++) {
        targetEl = targetEl.nextElementSibling;
        if(j == i) {
            return targetEl;
        }
    }
    return null;
}

/* Renderer modules proper? */
(function () {
    function handleEvents() {
        let buttons = document.getElementsByTagName('BUTTON');
        [].forEach.call(buttons, function(button) {

            let action = button.getAttribute('data-action');
            button.addEventListener('click', function() {
                stuff['activeEl'] = button;
            }, false);
            button.addEventListener('click', stuff[action], false);
        });
    }

    function clearViews() {
         /* Close open editor tabs. */
        let editorTabs = document.querySelectorAll('.nav-link');
        [].forEach.call(editorTabs, function(editorTab) {
            editorTab.classList.remove('active');
        });

        /* Hide all page editors */
        let editors = document.querySelectorAll('.editor');
        [].forEach.call(editors, function(editor, i) {
            editor.style.display = 'none';
        });
    }

    function toggler() {
        let button = stuff['activeEl'];
        let target = document.querySelector(button.getAttribute('data-target'));

        if (target.style.display == 'block') {
            clearViews();
            target.style.display = 'none';
            button.classList.remove('active');
        } else if (target.style.display == 'none') {
            clearViews();

            if (target.id == 'code') {
                populateCodeEditor();
            } else if (target.id == 'full-editor') {
                populateEditor();
            } else if (target.id == 'preview') {
                populatePreview();
            }

            target.style.display = 'block';
            button.classList.add('active');
        }
    }

    function populateCodeEditor() {
        let editorHTML = document.querySelector('.ql-editor').innerHTML;
        let codeHTML = document.querySelector('#codetext');
        
        codeHTML.value = editorHTML;
    }

    function refreshStats() {
        document.querySelector('#wordCount').innerHTML = quill.getText(0).split(' ').length;
        document.querySelector('#characterCount').innerHTML = quill.getText(0).length;
    }

    function populateEditor() {
        let codeHTML = document.querySelector('#codetext').value;

        quill.setText('');
        quill.clipboard.dangerouslyPasteHTML(0, codeHTML);
    }

    function populatePreview() {
        let editorHTML = document.querySelector('.ql-editor');
        let previewHTML = document.getElementById('preview');

        previewHTML.innerHTML = editorHTML.innerHTML;
    }

    function toggleSidebar() {
        let button = stuff['activeEl'];
        let target = document.querySelector(button.getAttribute('data-target'));
        
        let listObject = stuff[button.getAttribute('data-list')]();
        populateList(target, listObject);            
        previousElementSibling(target).querySelector('.badge-pill').innerHTML = Object.keys(listObject).length;

        if (target.style.display == 'block') {
            target.style.display = 'none';
        } else if (target.style.display == 'none') {
            target.style.display = 'block';                
        }
    }

    function toggleTabs(disabled) {
        let tabs = document.querySelectorAll('.nav-link');
        
        if(disabled == true) {
            [].forEach.call(tabs, function(tab, i) {
                tab.disabled = true;
            });
        } else if(disabled == false) {
            [].forEach.call(tabs, function(tab, i) {
                tab.disabled = false;
            });
        }
    }

    function populateList(target, listObject) {
        clearList(target);

        for(i in listObject) {
            target.innerHTML = target.innerHTML + '<li><a class="file-link" href="#' + listObject[i].permalink + '" >' + listObject[i].title + '</a></li>';
        }

        let list = target.querySelectorAll('.file-link');
        [].forEach.call(list, function(item, i){
            item.addEventListener('click', loadData, false);
        });
    }

    function clearList(targetList) {
        targetList.innerHTML = '';
    }

    function toggleSave() {
        let saveClose = document.querySelector('#saveCloseButtons');   

        if(saveClose.style.display == 'block') {
            saveClose.style.display = 'none';
        } else if(saveClose.style.display == 'none') {
            saveClose.style.display = 'block';
        }
    }

    function setTitle(newTitle, saved) {
        let title = document.querySelector('#edittitle');

        if(saved == true) {
            title.innerHTML = newTitle;
        } else if(saved == false) {
            title.innerHTML = '<em>' + newTitle + '<span class="unsaved"></span></em>';
        }
    }

    function loadData() {
        setTimeout(function() {
            let file = location.href.split('#')
            console.log(file[1]);

            let fileData = getPages();

            /* Clear Editors */
            clearViews();

            /* Enable Tabs */
            toggleTabs(false);

            /* Reset Title */
            setTitle(fileData[file[1]].title, true);

            /* Display Content Editor */
            document.getElementById('full-editor').style.display = 'block';
            
            /* Populate Content */
            quill.setText('');
            quill.clipboard.dangerouslyPasteHTML(0, fileData[file[1]].content);
            populateCodeEditor();
            refreshStats();

        }, 50);
    }

    function watchEditors() {
        /* Add Editor Event Listeners */
        let codeEditor = document.querySelector('#codetext');
        codeEditor.addEventListener('keyup', function() {
            populateEditor();
            populatePreview();
            refreshStats();
        });

        let editor = document.querySelector('.ql-editor');
        editor.addEventListener('input', function() {
            populateCodeEditor();                
            populatePreview();
            refreshStats();
        });
    }

    function initialize() {
        clearViews();

        /* Hide active sidebar lists. */
        let sidebarLists = document.querySelectorAll('.item-list');
        [].forEach.call(sidebarLists, function(list, i) {
            list.style.display = 'none';
        });

        if(stuff.loggedIn == false) {
            /* Disable editor tabs */
            toggleTabs(true);

            /* Hide Save/Cancel buttons */
            toggleSave();

            setTitle('Logged Out. <a href="#">Sign In</a>', true);

        } else if(stuff.loggedIn == true) {
            /* Disable editor tabs */
            toggleTabs(false);

            /* Start watching editors */
            watchEditors();

            /* Load opened file content */
            setTitle('Now Editing: Opened File', true);

            /* Populate code editor if content loaded. */
            populateCodeEditor(); 
        }
    }

    function getPosts() {
        if(stuff.loggedIn == true ) {
            let listObject = {
                pageOne: {
                    title: 'Page 1: Edited',
                    permalink: 'pageOne',
                    content: stuff.dummyContent
                },
                pageTwo: {
                    title: 'Page 2: Edited',
                    permalink: 'pageTwo',
                    content: stuff.dummyContent
                }
            };

            return listObject;
        } else {
            return null;
        }
    }

    function getPages() {
        if(stuff.loggedIn == true ) {
            let listObject = {
                pageOne: {
                    title: 'Page 1: Edited',
                    permalink: 'pageOne',
                    content: stuff.dummyContent
                },
                pageTwo: {
                    title: 'Page 2: Edited',
                    permalink: 'pageTwo',
                    content: stuff.dummyContentTwo
                }
            };

            return listObject;
        } else {
            return null;
        }
    }

    function clearEditors() {
        quill.setText('');
    }

    function save() {

    }

    function cancel() {
        clearViews();
        toggleTabs(true);
        let editorTabs = document.querySelectorAll('.nav-link');
        [].forEach.call(editorTabs, function(editorTab) {
            editorTab.classList.remove('active');
        });
        setTitle('', false);
        toggleSave();
    }

    function triggerLogin() {

    }

    let stuff = {
        'loggedIn': true,
        'toggleSidebar': toggleSidebar,
        'toggler': toggler,
        'activeEl': '',
        'triggerLogin': triggerLogin,
        'save': save,
        'postList': getPosts,
        'pageList': getPages,
        'dummyContent':'<h1>This is dummy content</h1><p>here is dummy text</p>',
        'dummyContentTwo':'<h1>This is more dummy content</h1><p>here is dummy text</p>',
        'cancel': cancel
    };
    
    handleEvents();
    initialize();
})();